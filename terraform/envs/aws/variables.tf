variable "ssh_key" {
  type    = string
  default = "AAAAB3NzaC1yc2EAAAADAQABAAABAQDZVcXCCecgkTV118wpGucMO+QI6QOmBXunycSc4lq6n9561xxSxsaqERwtIV8puxiWxsenBr91VKJqAC5Gfk8RIkvJJiOcWpXhqM/gfl4WiRl79b5h+gCg40SxeUgC53phAkEWPOFwr+lD2zfATqMSsxXlWz6v+GTnKklOL+Zy5XSyJFswfuRdViG/0b77hvtAT8Kg2E5xWvaJIOe4A2ShZSDC5i/u7AaE0yP74Fzw7WwgJ+q+cB5Kx8OaBr4N18UiBKqpcuOTPqAcn0JIcI2dCGuDBE5TkVyZ5E+ATRjb/77KiWtMWl3rSLBmBae6VlX6pyUWsU5LAnte0gvWLz1n"
}

variable "tags" {
  type = map(any)

  default = {
    Environment = "sandbox"
    ManagedBy   = "ansible"
    CreatedBy   = "Terraform"
  }
}

variable "az" {
  type    = string
  default = "eu-central-1c"
}
